/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.oxprogram;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OXGame {
    static char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'X';
    static int row,col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;
    
    public static void main(String[] args) {
        showWelcome();
        while(true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
        if(finish) {
            break;
        }
        }
    }
    
    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r=0;r<table.length;r++) {
            for (int c=0;c<table[r].length;c++) {
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
        
    }
    
     public static void showTurn() {
         System.out.println("Turn "+currentPlayer);
     }
     
     public static void inputRowCol() {
         System.out.println("Please input row, col: ");
         row = kb.nextInt();
         col = kb.nextInt();
     }
     
     public static void process() {
         if (setTable()) {
             if(checkWin()) {
                 finish = true;
                 showResult();
                 return;
             }
             if(checkDraw()) {
                 finish = true;
                 showDraw();
                 return;
             }
             count++;
             switchPlayer();
         }
     }
     
     public static boolean setTable() {
         table[row-1][col-1] = currentPlayer;
         return true;
     }
     
     public static void switchPlayer() {
         if (currentPlayer == 'X') {
             currentPlayer='O';
         }else{
             currentPlayer = 'X';
         }
     }
     
     public static boolean checkWin() {
         if(checkVertical()) {
            return true;
         }else if (checkHorizontal()) {
             return true;
         }else if (checkLeftDiagnal()){
             return true;
         }else if (checkRightDiagnal()) {
             
         }
         return false;
     }

    public static boolean checkVertical() {
        for(int r=0;r<table.length;r++) {
            if(table[r][col-1]!=currentPlayer) 
                return false;
        }
        return true;
    }

    public static boolean checkHorizontal() {
        for(int c=0;c<table.length;c++) {
            if(table[row-1][c]!=currentPlayer) 
                return false;
        }
        return true;
    }
    
    public static boolean checkDiagnal() {
        if(checkLeftDiagnal()) {
            return true;
        }else if(checkRightDiagnal()) {
            return true;
        }
        return false;
    }

    public static boolean checkLeftDiagnal() {
        for(int i=0;i<table.length;i++) {
            if(table[i][i] != currentPlayer)
                return false;
        }
        return true;
    }

    public static boolean checkRightDiagnal() {
        for(int i=0;i<table.length;i++) {
            if(table[i][2-i] != currentPlayer)
                return false;
        }
        return true;
    }
    
    public static void showResult() {
        showTable();
        System.out.println(">>>"+currentPlayer+" Win<<<");
    }

    public static boolean checkDraw() {
        if(count==8) {
            return true;
        }
        return false;
    }

    public static void showDraw() {
        System.out.println(">>>Draw<<<");
    }
}
